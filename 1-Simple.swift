protocol Diffable {
  associatedtype Patch
  static func diff(_ a: Self, _ b: Self) -> Patch
  static func patch(_ a: Self, with: Patch) -> Self?
}

struct DiffableEquatable<Eq: Equatable>: Equatable, Diffable {
  let equatable: Eq

  enum Patch {
    case noop(DiffableEquatable)
    case set(DiffableEquatable, to: DiffableEquatable)
  }

  static func diff(_ a: DiffableEquatable, _ b: DiffableEquatable) -> Patch {
    return a != b ? .set(a, to: b) : .noop(a)
  }
  static func patch(_ a: DiffableEquatable, with patch: Patch) -> DiffableEquatable? {
    switch patch {
    case let .noop(pre) where a == pre: return a
    case let .set(pre, b) where a == pre: return b
    default: return nil
    }
  }

  static func ==(_ lhs: DiffableEquatable, rhs: DiffableEquatable) -> Bool {
    return lhs.equatable == rhs.equatable
  }
}

extension DiffableEquatable: CustomDebugStringConvertible {
  var debugDescription: String {
    return String(reflecting: equatable)
  }
}
extension DiffableEquatable.Patch: CustomDebugStringConvertible {
  var debugDescription: String {
    switch self {
    case .noop(_): return "id"
    case let .set(a, b): return String(reflecting: a.equatable) + " ↦ " + String(reflecting: b.equatable)
    }
  }
}

func diff<Eq: Equatable>(_ a: Eq, _ b: Eq) -> DiffableEquatable<Eq>.Patch {
  return DiffableEquatable.diff(DiffableEquatable(equatable: a), DiffableEquatable(equatable: b))
}

func patch<Eq: Equatable>(_ a: Eq, with patch: DiffableEquatable<Eq>.Patch) -> Eq? {
  return DiffableEquatable.patch(DiffableEquatable(equatable: a), with: patch)?.equatable
}

diff(false, false)
diff(false, true)
