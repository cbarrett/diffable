protocol Diffable {
  associatedtype Patch: PatchProtocol where Patch.Base == Self
  static func diff(_ src: Self, _ dst: Self) -> Patch
  static func patch(_ src: Self, with: Patch) -> Self?
}

protocol PatchProtocol {
  associatedtype Base
  var apply: (Base) -> Base? { get }
}

protocol NoopPatch: PatchProtocol {
  static func noop(src: Base) -> Self
}

protocol SetPatch: PatchProtocol {
  static func set(src: Base, dst: Base) -> Self
}

protocol RecPatch: PatchProtocol {
  associatedtype SubPatch: PatchProtocol
  static func rec(src: Base, sub: SubPatch) -> Self
}

struct EquatablePatch<Base: Equatable>: NoopPatch, SetPatch, CustomDebugStringConvertible {
  let debugDescription: String
  let apply: (Base) -> Base?

  static func noop(src: Base) -> EquatablePatch<Base> {
    let str = String(reflecting: src) + "_id"
    return EquatablePatch(debugDescription: str) { $0 == src ? $0 : nil }
  }
  static func set(src: Base, dst: Base) -> EquatablePatch<Base> {
    let str = String(reflecting: src) + " ↦ " + String(reflecting: dst)
    return EquatablePatch(debugDescription: str) { $0 == src ? dst : nil }
  }
}

struct OptionalPatch<SubPatch: PatchProtocol>: NoopPatch, SetPatch, RecPatch, CustomDebugStringConvertible {
  typealias Base = Optional<SubPatch.Base>
  let debugDescription: String
  let apply: (Base) -> Base?

  static func noop(src: Base) -> OptionalPatch<SubPatch> {
    if let src = src {
      let str = "some_id ◃ " + String(reflecting: src)
      return OptionalPatch(debugDescription: str) { $0 != nil ? $0 : nil }
    } else {
      return OptionalPatch(debugDescription: "nil_id") { $0 == nil ? nil : nil }
    }
  }
  static func set(src: Base, dst: Base) -> OptionalPatch<SubPatch> {
    if let src = src {
      let str = "some(" + String(reflecting: src) + ") ↦ " + String(reflecting: dst)
      return OptionalPatch(debugDescription: str) { $0 != nil ? dst : nil }
    } else {
      let str = "nil ↦ " + String(reflecting: dst)
      return OptionalPatch(debugDescription: str) { $0 == nil ? dst : nil }
    }
  }
  static func rec(src: Base, sub: SubPatch) -> OptionalPatch<SubPatch> {
    if src != nil {
      let str = "some_id ◃ " + String(reflecting: sub) + ")"
      return OptionalPatch(debugDescription: str) { $0 != nil ? sub.apply($0!) : nil }
    } else {
      return OptionalPatch(debugDescription: "nil_id") { $0 == nil ? nil : nil }
    }
  }
}

extension Equatable {
  static func diff(_ src: Self, _ dst: Self) -> EquatablePatch<Self> {
    return src != dst ? .set(src: src, dst: dst) : .noop(src: src)
  }
  static func patch(_ src: Self, with patch: EquatablePatch<Self>) -> Self? {
    return patch.apply(src)
  }
}

extension Bool: Diffable {}

extension Optional: Diffable where Wrapped: Diffable {
  typealias Patch = OptionalPatch<Wrapped.Patch>
  static func diff(_ src: Optional<Wrapped>, _ dst: Optional<Wrapped>) -> Patch {
    if let src = src, let dst = dst {
      return .rec(src: .some(src), sub: Wrapped.diff(src, dst))
    } else if src != nil {
      return .set(src: src, dst: nil)
    } else if dst != nil {
      return .set(src: nil, dst: dst)
    } else {
      return .noop(src: nil)
    }
  }
  static func patch(_ src: Optional<Wrapped>, with patch: Patch) -> Optional<Wrapped>? {
    return patch.apply(src)
  }
}

Optional<Bool>.diff(true, true)
Optional<Bool>.diff(true, false)
Optional<Bool>.diff(true, nil)

