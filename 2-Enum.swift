protocol Diffable {
  associatedtype Patch
  static func diff(_ a: Self, _ b: Self) -> Patch
  static func patch(_ a: Self, with: Patch) -> Self?
}

extension Diffable where Self: Equatable, Patch == EquatablePatch<Self> {
  static func diff(_ a: Self, _ b: Self) -> Patch {
    return a != b ? .set(a, to: b) : .noop(a)
  }
  static func patch(_ a: Self, with patch: Patch) -> Self? {
    switch patch {
    case let .noop(pre) where a == pre: return a
    case let .set(pre, b) where a == pre: return b
    default: return nil
    }
  }
}

enum EquatablePatch<T> {
  case noop(T)
  case set(T, to: T)
}
extension EquatablePatch: CustomDebugStringConvertible {
  var debugDescription: String {
    switch self {
    case .noop(_): return "id"
    case let .set(a, b): return String(reflecting: a) + " ↦ " + String(reflecting: b)
    }
  }
}

extension Bool: Diffable {
  typealias Patch = EquatablePatch<Bool>
}

Bool.diff(false, false)
Bool.diff(false, true)
Bool.diff(true, false)
Bool.diff(true, true)

Bool.patch(false, with: Bool.diff(false, false))
Bool.patch(false, with: Bool.diff(false, true))
Bool.patch(false, with: Bool.diff(true, false))
Bool.patch(false, with: Bool.diff(true, true))
Bool.patch(true, with: Bool.diff(false, false))
Bool.patch(true, with: Bool.diff(false, true))
Bool.patch(true, with: Bool.diff(true, false))
Bool.patch(true, with: Bool.diff(true, true))
