protocol Diffable {
  associatedtype Patch: PatchProtocol where Patch.Base == Self
  static func diff(_ src: Self, _ dst: Self) -> Patch
  static func patch(_ src: Self, with: Patch) -> Self?
}

protocol PatchProtocol {
  associatedtype Base
}

protocol NoopPatch: PatchProtocol {
  static func noop(src: Base) -> Self
}

protocol SetPatch: PatchProtocol {
  static func set(src: Base, dst: Base) -> Self
}

struct EquatablePatch<Base: Equatable>: NoopPatch, SetPatch, CustomDebugStringConvertible {
  let debugDescription: String
  let apply: (Base) -> Base?

  static func noop(src: Base) -> EquatablePatch<Base> {
    let str = String(reflecting: src) + "_id"
    return EquatablePatch(debugDescription: str) { $0 == src ? $0 : nil }
  }
  static func set(src: Base, dst: Base) -> EquatablePatch<Base> {
    let str = String(reflecting: src) + " ↦ " + String(reflecting: dst)
    return EquatablePatch(debugDescription: str) { $0 == src ? dst : nil }
  }
}

extension Diffable where Self: Equatable, Patch == EquatablePatch<Self> {
  static func diff(_ src: Self, _ dst: Self) -> Patch {
    return src != dst ? .set(src: src, dst: dst) : .noop(src: src)
  }
  static func patch(_ src: Self, with patch: Patch) -> Self? {
    return patch.apply(src)
  }
}

extension Bool: Diffable {
  typealias Patch = EquatablePatch<Bool>
}

Bool.diff(false, false)
Bool.diff(false, true)
Bool.diff(true, false)
Bool.diff(true, true)

Bool.patch(false, with: Bool.diff(false, false))
Bool.patch(false, with: Bool.diff(false, true))
Bool.patch(false, with: Bool.diff(true, false))
Bool.patch(false, with: Bool.diff(true, true))
Bool.patch(true, with: Bool.diff(false, false))
Bool.patch(true, with: Bool.diff(false, true))
Bool.patch(true, with: Bool.diff(true, false))
Bool.patch(true, with: Bool.diff(true, true))
